import React from 'react';
import data from '../mockups/data';
import {Link} from 'react-router-dom';

class Product extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      products: []
    };
  }

  componentDidMount() {
    this.setState({
      products: Object.values(data)
    });
  }

  render() {
    const {products} = this.state;
    return (
      <div>
        <ul>
          {products.map(item => (
            <li key={item.id}>
              <Link to={`/products/${item.id}`}>{item.name}</Link></li>
          ))}
        </ul>
      </div>
    )
  }
}

export default Product;