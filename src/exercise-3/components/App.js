import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Route, NavLink, Switch, Redirect} from 'react-router-dom';
import Home from './Home';
import Profile from './Profile';
import About from './About';
import Product from './Product';
import ProductDetail from './ProductDetail';

class App extends Component {
  render() {
    return (
      <div className='app'>
        <Router>
          <header className='app-header'>
            <nav>
              <NavLink exact activeClassName='link-active' to='/'>Home</NavLink>
              <NavLink exact activeClassName='link-active' to='/products'>Products</NavLink>
              <NavLink activeClassName='link-active' to='/my-profile'>Profile</NavLink>
              <NavLink activeClassName='link-active' to='/about-us'>About</NavLink>
            </nav>
          </header>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route exact path='/products' component={Product}></Route>
              <Route path='/products/:id' component={ProductDetail}></Route>
              <Route path='/my-profile' component={Profile}/>
              <Route path='/about-us' component={About}/>
              <Route path='/goods' render={() => (
                <Redirect to='/products'/>
              )} />
              <Route path='*' render={() => (
                <Redirect to='/'/>
              )}/>
            </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
