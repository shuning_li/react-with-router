import React from 'react';
import data from '../mockups/data';

const ProductDetail = props => {
  const { id } = props.match.params;
  const itemDetail = Object.values(data).find(v => v.id === parseInt(id));
  return (
    <div>
      <h4>Product Detail</h4>
      <ul>
        <li>Name: {itemDetail.name}</li>
        <li>id: {itemDetail.id}</li>
        <li>Price: {itemDetail.price}</li>
        <li>Quantity: {itemDetail.quantity}</li>
        <li>Desc: {itemDetail.desc}</li>
        <li>URl: {props.history.location.pathname}</li>
      </ul>
    </div>
  )
};

export default ProductDetail;