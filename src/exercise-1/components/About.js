import React from 'react';
import {Link} from 'react-router-dom';

const About = () => {
  return (
    <section>
      view our <Link to="/">website</Link>
    </section>
  )
};

export default About;