import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Route, NavLink} from 'react-router-dom';
import Home from './Home';
import Profile from './Profile';
import About from './About';

class App extends Component {
  render() {
    return (
      <div className='app'>
        <Router>
          <header className='app-header'>
            <nav>
              <NavLink exact activeClassName='link-active' to='/'>Home</NavLink>
              <NavLink activeClassName='link-active' to='/my-profile'>Profile</NavLink>
              <NavLink activeClassName='link-active' to='/about-us'>About</NavLink>
            </nav>
          </header>
            <Route exact path='/' component={Home} />
            <Route path='/my-profile' component={Profile}/>
            <Route path='/about-us' component={About}/>
        </Router>
      </div>
    );
  }
}

export default App;
